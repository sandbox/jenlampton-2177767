<?php

/**
 * @file
 * Administration pages for Basic Cart promo codes.
 */

/**
 * Menu callback.
 */
function basic_cart_promocode_admin() {
  // Form to add a new promo code at the top.
  $output = drupal_render(drupal_get_form('basic_cart_promocode_edit'));

  // List of existing promo codes.
  $rows = array();
  $options = basic_cart_promocode_options();
  $result = db_query("SELECT * FROM {basic_cart_promocode}")->fetchAllAssoc('id');
  foreach ($result as $record) {
    $rows[] = array(
      'id' => $record->id,
      'code' => check_plain($record->code),
      'type' => $options[$record->type],
      'amount' => basic_cart_promocode_amount_format($record),
      'edit' => l(t('edit'), 'admin/config/basic_cart/promocodes/' . $record->id . '/edit'),
      'delete' => l(t('delete'), 'admin/config/basic_cart/promocodes/' . $record->id . '/delete'),
    );
  }
  $header = array(
    array('data' => t('ID'), 'field' => 'id'),
    array('data' => t('Promo code'), 'field' => 'code'),
    array('data' => t('Discount type'), 'field' => 'type'),
    array('data' => t('Amount'), 'field' => 'amount'),
    array('data' => t('Operations'), 'colspan' => 2),
  );

  $output .= theme('table', array('header' => $header, 'rows' => $rows));

  return $output;
}

function basic_cart_promocode_edit($form, &$form_state, $id = NULL) {
  if ($id != NULL) {
    $promocode = basic_cart_promocode_load($id);
    if ($promocode === FALSE) {
      $id = NULL;
    }
  }

  $form = array();
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id ? $promocode->id : 0,
  );
  $form['code'] = array(
    '#title' => t('Code'),
    '#type' => 'textfield',
    '#required' => TRUE,
    '#description' => t('Promotional code to be entered at checkout.'),
    '#default_value' => $id ? $promocode->code : '',
  );
  $form['type'] = array(
    '#type' => 'radios',
    '#title' => t('Type of discount'),
    '#required' => TRUE,
    '#options' => basic_cart_promocode_options(),
    '#default_value' => $id ? $promocode->type : array('none'),
  );
  $form['amount'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#title' => t('Discount amount'),
    '#description' => t('Enter 10.00 for fixed or 10% for percent.'),
    '#default_value' => $id ? $promocode->amount : '',
    '#states' => array(
      'visible' => array(
        ':input[name=type]' => array('!value' => 'none'),
      ),
    ),
  );

  if ($id != NULL) {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Save'),
    );
    $form['delete'] = array(
      '#type' => 'submit',
      '#value' => t('Delete'),
    );
  }
  else {
    $form['submit'] = array(
      '#type' => 'submit',
      '#value' => t('Add promo code'),
    );
  }

  return $form;
}

/**
 * Validation handler for promo code add form.
 */
function basic_cart_promocode_edit_validate(&$form, &$form_state) {
  // Check for unique code.
  $id = db_query("SELECT id FROM {basic_cart_promocode} WHERE code = :code", array(':code' => $form_state['values']['code']))->fetchField();
  if (is_numeric($id) && $id != $form_state['values']['id']) {
    $message = t('This code already exists. Promo codes must be unique, please choose another.');
    form_set_error('code', $message);
  }

  // Check for precent / fixed mismatch.
  if (($form_state['values']['type'] == 'percent') && strstr($form_state['values']['amount'], '$')) {
    $message = t('Percentage promo codes must have a percentage discount amount.');
    form_set_error('amount', $message);
  }
  else if ($form_state['values']['type'] == 'fixed') {
    // Check for a fxed / percent mismatch.
    if (strstr($form_state['values']['amount'], '%')) {
      $message = t('Fixed amount promo codes must have a fixed amount discount amount.');
      form_set_error('amount', $message);
    }
    if (strstr($form_state['values']['amount'], ".")) {
      // Check for too many decimals and throw an error.
      if (strlen(substr(strrchr($form_state['values']['amount'], "."), 1)) > 2) {
        $message = t('Please include no more than 2 decimal places for cents.');
        form_set_error('amount', $message);
      }
      // Check for too few decimals and just add zeros.
      else if (strlen(substr(strrchr($form_state['values']['amount'], "."), 1)) < 2) {
        $form_state['values']['amount'] .= '0';
        // One more time...
        if (strlen(substr(strrchr($form_state['values']['amount'], "."), 1)) < 2) {
          $form_state['values']['amount'] .= '0';
        }
      }
    }
    else {
      // Add decimals.
      $form_state['values']['amount'] .= '.00';
    }
  }
}

/**
 * Submit handler for promo code add form.
 */
function basic_cart_promocode_edit_submit(&$form, &$form_state) {
  // Check to see if the delete button was clicked.
  if ($form_state['clicked_button']['#value'] == 'Delete') {
    drupal_goto('admin/config/basic_cart/promocodes/' . $form_state['values']['id'] . '/delete');
  }

  // Remove all characters from the amount before saving.
  $form_state['values']['amount'] = preg_replace("/[^0-9,.]/", "", $form_state['values']['amount']);

  // Insert this code into the database.
  db_merge('basic_cart_promocode')
    ->key(array('id' => $form_state['values']['id']))
    ->fields(array(
      'type' => $form_state['values']['type'],
      'amount' => $form_state['values']['amount'],
      'code' => $form_state['values']['code'],
    ))
    ->execute();

  $form_state['redirect'] = 'admin/config/basic_cart/promocodes';
}

/**
 * Confirmation form for deleting a promo code.
 */
function basic_cart_promocode_delete_confirm($form, &$form_state, $id) {
  $promocode = basic_cart_promocode_load($id);

  $form['#code'] = $promocode;
  $form['id'] = array(
    '#type' => 'value',
    '#value' => $id,
  );

  return confirm_form($form, t('Are you sure you want to delete the promo code %title?', array('%title' => $promocode->code)), 'admin/config/basic_cart/promocodes', t('This action cannot be undone.'), t('Delete'), t('Cancel'));
}

/**
 * Confirm form submit handler.
 */
function basic_cart_promocode_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $promocode = basic_cart_promocode_load($form_state['values']['id']);
    basic_cart_promocode_delete($form_state['values']['id']);
    watchdog('basic_cart_promocode', 'deleted %title.', array('%title' => $promocode->code));
    drupal_set_message(t('promo code %title has been deleted.', array('%title' => $promocode->code)));
  }

  $form_state['redirect'] = 'admin/config/basic_cart/promocodes';
}

/**
 * Helper function: returns all available promo code types.
 *   @return array
 *     Keyed options for promo code types.
 */
function basic_cart_promocode_options() {
  $options = array(
    'none' => t('No discount'),
    'fixed' => t('Fixed amount discount'),
    'percent' => t('Percentage discount'),
  );

  drupal_alter('basic_cart_promocode_options', $options);

  return $options;
}
