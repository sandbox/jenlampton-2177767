<?php

/**
 * @file
 * API documentation for Basic Cart Promo code module.
 */

/**
 * Change the types of promo codes available to the system.
 *
 * @param array $options
 *   A keyed array of promocode types. Passed by reference.
 */
function hook_basic_cart_promocode_options_alter(&$options) {
  
}
